# Git Commands

## Error : Pull : You have not concluded your merge (MERGE_HEAD exists)

Usually happens when something go wrong in a merge after a commit

| Command           | Details                                     |
| ----------------- | ------------------------------------------- |
| git status        | To see current status                       |
| git merge --abort | Undo active merge - Since git version 1.7.4 |
| git reset --merge | Undo active merge                           |
| git pull          | Pull latest from origin                     |

## Hard reset local branch to origin

git reset --hard origin/<branch_name>

